import React, { Component } from "react";
import "../stylesheets/style.scss";
import styled from 'styled-components';

const Heading = styled.h1 `
	color: #000;
`;

// app component
export default class App extends Component {
	// render
	render() {
		return (
		<div className="main__container">
			<div className="container">
				<div className="col-md-12">
					<Heading>
						Cars for sale
					</Heading>
				</div>
			</div>

			{this.props.children}
		</div>
  		);
	}
}
