import React from "react";
import styled from 'styled-components';

// Car Detail component

const Price = styled.h5 `
	color: #000;
`;

const Make = styled.h5 `
	color: #000;
	display: inline-block;
	font-weight: bold;
	margin-right: 10px;
	margin-bottom: 0px;
`;

const Name = styled.h5 `
	color: #000;
	display: inline-block;
	font-weight: bold;
	margin-bottom: 0px;
`;

const CarImage = styled.img `
	width: auto;
	max-width: 500px;
	height: auto;
	display: block;
`;

export const CarDetail = ({ Car }) => {

	return (
		<div>
			{ Car &&
				<CarImage onError={ (e) => { e.target.src = 'https://dummyimage.com/600x400/000/ffffff&text=Missing+Image'}} src={Car.model && Car.model.imageUrl}></CarImage>
			}

			<Make>{Car.make && Car.make.name}</Make>
			<Name>{Car.model && Car.model.name}</Name>
			<Price>${Car.model && Car.model.price}</Price>
		</div>
	);
}
