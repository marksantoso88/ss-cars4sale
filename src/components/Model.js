import React, {Component} from "react";
import styled from 'styled-components';
import {Link} from 'react-router';
import {connect} from "react-redux";
import { CarDetail } from './CarDetail';


const SubHeading = styled.h4 `
	margin-top: 2em;
	color: #000;
`;

const Navigation = styled.nav `
	color: #000;
	width: 100%;
	background: #fafafa;
	padding: 1em;
	font-family: fantasy;
`;

const HomeBtn = styled(Link)`
	color: #000;
	width: 100px;
	background: green;
	color: #fff;
	padding: 1em;
	text-align: center;
	display: inline-block;
	margin-right: 50px;

	&:hover,
	&:visited,
	&:active,
	&:focus {
		color: #fff;
	}
`;

const SearchBtn = styled(Link)`
	color: #000;
	width: 100px;
	background: blue;
	color: #fff;
	padding: 1em;
	text-align: center;
	display: inline-block;

	&:hover,
	&:visited,
	&:active,
	&:focus {
		color: #fff;
	}

`;

// Model page component
class Model extends Component {

	componentWillMount() {
		this.props.dispatch({type: 'FETCH_MAKES'});
		this.props.dispatch({type: 'FETCH_MODELS'});
	}

	getCarDetails(id) {

		const car = {};
		for (const model of this.props.models) {

			if (model.id == id) {
				car.model = model;

				for (const make of this.props.makes) {
					if (make.id === model.makeId) {
						car.make = make;
					}
				}
			}
		}

		return car;
	}


	// render
	render() {

		const Car = this.getCarDetails(this.props.params.id);

		return (
				<div className="container">
					<div className="col-md-12">
						<Navigation>
							<HomeBtn to="/">Home</HomeBtn>
							<SearchBtn to="/search">Search</SearchBtn>
						</Navigation>

						<SubHeading>Car details</SubHeading>
						<CarDetail Car={Car} />
 					</div>
				</div>
		);
	}
}

// export the connected class
function mapStateToProps(state) {
	return {
		weekly: state.cars.weekly || [],
		makes: state.cars.makes || [],
		models: state.cars.models || []
	};
}
export default connect(mapStateToProps)(Model);
