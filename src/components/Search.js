import React, { Component } from "react";
import styled from 'styled-components';
import { Link } from 'react-router';
import { connect } from "react-redux";


const Heading = styled.h1 `
	color: #000;
`;

const SubHeading = styled.h4 `
	margin-top: 2em;
	color: #000;
`;

const ModelSelect = styled.select `
	color: #000;
	width: 100px;
	background: #fafafa;
	padding: 1em;
`;

const MakeSelect = styled.select `
	color: #000;
	width: 100px;
	background: #fafafa;
	padding: 1em;
`;

const Navigation = styled.nav `
	color: #000;
	width: 100%;
	background: #fafafa;
	padding: 1em;
	font-family: fantasy;
`;

const HomeBtn = styled(Link)`
	color: #000;
	width: 100px;
	background: green;
	color: #fff;
	padding: 1em;
	text-align: center;
	display: inline-block;
	margin-right: 50px;

	&:hover,
	&:visited,
	&:active,
	&:focus {
		color: #fff;
	}
`;

const SearchBtn = styled(Link)`
	color: #000;
	width: 100px;
	background: blue;
	color: #fff;
	padding: 1em;
	text-align: center;
	display: inline-block;

	&:hover,
	&:visited,
	&:active,
	&:focus {
		color: #fff;
	}
`;

const SearchButton = styled.button `
	background: red;
	border: 0;
	color: #fff;
	margin-left: 20px;
`;

// Home page component
class Search extends Component {

	constructor(props) {
		super(props);

		this.state = {
			models: null,
			model: null,
			searchDisabled: true
		}

		this.populateModels = this.populateModels.bind(this);
		this.handleSearch = this.handleSearch.bind(this);
	}

	componentWillMount() {
		this.props.dispatch({type: 'FETCH_MAKES'});
		this.props.dispatch({type: 'FETCH_MODELS'});
	}

	handleSearch(e) {
		this.props.history.push(`/make/model/${this.state.model}`);
	}

	populateModels(e) {

 		const modelList = [];

		for (const model of this.props.models) {
			if (model.makeId == e.target.value) {
				modelList.push({id: model.id, name: model.name});
			}
		}

		const models = modelList.map((model, index) => {
			return <option key={index} value={model.id} >{ model.name }</option>;
		});

 		this.setState({ models});
	}

	// render
	render() {
		return (
			<div className="container">
				<div className="col-md-12">
					<Navigation>
						<HomeBtn to="/">Home</HomeBtn>
						<SearchBtn to="/search">Search</SearchBtn>
					</Navigation>

					<SubHeading>Search</SubHeading>

					<select onChange={this.populateModels}>
						<option value="0" >Please select...</option>
						{ this.props.makes.map(function ( option ) {
							return <option key={option.id} value={ option.id } >{ option.name }</option>;
						})}
					</select>

					{this.state.models &&
						<select onChange={ (e) => { this.setState({model: e.target.value}); this.setState({ searchDisabled: false }); }} >
							<option value="0" >Please select...</option>
							{ this.state.models }
						</select>
					}

					<SearchButton disabled={this.state.searchDisabled} onClick={this.handleSearch}>
						Search
					</SearchButton>
				</div>
			</div>
		);
	}
}

// export the connected class
function mapStateToProps(state) {
	return {
		makes: state.cars.makes || [],
		models: state.cars.models || []
	};
}
export default connect(mapStateToProps)(Search);
