// cars reducer

export default function carsReducer(state = {}, action) {

	switch (action.type) {
		case 'STORE_WEEKLY_CAR':
			return { ...state, ...action.promo };

		case 'STORE_MAKES':
			return { ...state, ...action.makes };

		case 'STORE_MODELS':
			return { ...state, ...action.models };
			
		// initial state
		default:
			return state;
	}
}
