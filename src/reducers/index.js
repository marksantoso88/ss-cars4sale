import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux";
import { reducer as formReducer } from "redux-form";
import carsReducer from './carsReducer';
// main reducers
const rootReducer = combineReducers({
  routing: routerReducer,
  form: formReducer,
  cars: carsReducer// your reducer here
});


export default rootReducer;
