import React from "react";
import { Router, Route, IndexRoute } from "react-router";
import { history } from './services';

import App from "./components/App";
import Home from "./components/Home";
import Search from "./components/Search";
import NotFound from "./components/NotFound";
import Model from "./components/Model";

// build the router

export default () => {
    const router = (
      <Router onUpdate={() => window.scrollTo(0, 0)} history={history}>
        <Route path="/" component={App}>
          <IndexRoute component={Home}/>
          <Route path="/search" component={Search}/>
          <Route path="/make/model/:id" component={Model}/>
          <Route path="*" component={NotFound}/>
        </Route>
      </Router>
    );

    return router;
}
