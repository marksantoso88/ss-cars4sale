/* Libraries */
import {
	call,
	put
} from "redux-saga/effects";

/* JSON */
import carOfTheWeek from '../assets/data/carOfTheWeek.json';
import carMakes from '../assets/data/makes.json';
import carModels from '../assets/data/models.json';

export function* FetchCarWeek() {
	// save the car in state
    const weekly = {};
    weekly['weekly'] = carOfTheWeek[0];

	yield put({
		type: 'STORE_WEEKLY_CAR',
		promo: weekly,
	});
}

// fetch the car of the week
export function* FetchMakes() {
		// save the makes in state
        const makes = {};
        makes['makes'] = carMakes;

		yield put({
			type: 'STORE_MAKES',
			makes: makes,
		});
}

// fetch the car of the week
export function* FetchModels() {

        const models = {};
        models['models'] = carModels;

		// save the models in state
		yield put({
			type: 'STORE_MODELS',
			models: models,
		});
}



export default [FetchCarWeek, FetchMakes, FetchModels];
