import { takeLatest } from "redux-saga";
import { fork } from "redux-saga/effects";
import { FetchCarWeek, FetchMakes, FetchModels } from "./cars";

// main saga generators
export default function* sagas() {
  yield [
    fork(takeLatest, 'FETCH_WEEKLY_CAR', FetchCarWeek),
    fork(takeLatest, 'FETCH_MAKES', FetchMakes),
    fork(takeLatest, 'FETCH_MODELS', FetchModels),
  ];
}
